package com.dionis.tutorial.dao;

import java.util.List;

import com.dionis.tutorial.dao.interfaces.QuestionDAOInterface;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;

import com.dionis.tutorial.model.Question;

public class QuestionDAO implements QuestionDAOInterface<Question, Integer> {

    private Session currentSession;

    private Transaction currentTransaction;

    public QuestionDAO() {
    }

    public Session openCurrentSession() {
        currentSession = getSessionFactory().openSession();
        return currentSession;
    }

    public Session openCurrentSessionwithTransaction() {
        currentSession = getSessionFactory().openSession();
        currentTransaction = currentSession.beginTransaction();
        return currentSession;
    }

    public void closeCurrentSession() {
        currentSession.close();
    }

    public void closeCurrentSessionwithTransaction() {
        currentTransaction.commit();
        currentSession.close();
    }

    private static SessionFactory getSessionFactory() {
        Configuration configuration = new Configuration().configure();
        StandardServiceRegistryBuilder builder = new StandardServiceRegistryBuilder()
                .applySettings(configuration.getProperties());
        configuration.addAnnotatedClass(Question.class);
        SessionFactory sessionFactory = configuration.buildSessionFactory(builder.build());
        return sessionFactory;
    }

    public Session getCurrentSession() {
        return currentSession;
    }

    public void setCurrentSession(Session currentSession) {
        this.currentSession = currentSession;
    }

    public Transaction getCurrentTransaction() {
        return currentTransaction;
    }

    public void setCurrentTransaction(Transaction currentTransaction) {
        this.currentTransaction = currentTransaction;
    }

    public void persist(Question entity) {
        try {
            getCurrentSession().save(entity);
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    public void update(Question entity) {
        getCurrentSession().update(entity);
    }

    public Question findById(Integer id) {
        Question question = (Question) getCurrentSession().get(Question.class, id);
        return question;
    }

    public void delete(Question entity) {
        getCurrentSession().delete(entity);
    }


    @SuppressWarnings("unchecked")
    public List<Question> findAll() {
        List<Question> questions = (List<Question>) getCurrentSession().createQuery("from Question").list();
        return questions;
    }

    public void deleteAll() {
        List<Question> entityList = findAll();
        for (Question entity : entityList) {
            delete(entity);
        }

    }
}
