package com.dionis.tutorial.services;

import com.dionis.tutorial.dao.QuestionDAO;
import com.dionis.tutorial.model.Question;

import java.util.List;

public class QuestionService {

    private static QuestionDAO questionDao;

    public QuestionService() {
        questionDao = new QuestionDAO();
    }

    public void persist(Question entity) {
        questionDao.openCurrentSessionwithTransaction();
        questionDao.persist(entity);
        questionDao.closeCurrentSessionwithTransaction();
    }

    public void update(Question entity) {
        questionDao.openCurrentSessionwithTransaction();
        questionDao.update(entity);
        questionDao.closeCurrentSessionwithTransaction();
    }

    public Question findById(Integer id) {
        questionDao.openCurrentSession();
        Question question = questionDao.findById(id);
        questionDao.closeCurrentSession();
        return question;
    }

    public void delete(Integer id) {
        questionDao.openCurrentSessionwithTransaction();
        Question question = questionDao.findById(id);
        questionDao.delete(question);
        questionDao.closeCurrentSessionwithTransaction();
    }

    public List<Question> findAll() {
        questionDao.openCurrentSession();
        List<Question> questions = questionDao.findAll();
        questionDao.closeCurrentSession();
        return questions;
    }

    public void deleteAll() {
        questionDao.openCurrentSessionwithTransaction();
        questionDao.deleteAll();
        questionDao.closeCurrentSessionwithTransaction();
    }

    public QuestionDAO questionDao() {
        return questionDao;
    }
}