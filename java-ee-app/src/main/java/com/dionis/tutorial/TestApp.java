package com.dionis.tutorial;

import com.dionis.tutorial.model.Question;
import com.dionis.tutorial.services.QuestionService;

import java.util.List;

public class TestApp {

    public static void main(String[] args) {
        QuestionService questionService = new QuestionService();
        Question question1 = new Question(1, "Java Generics");
        Question question2 = new Question(2, "Java Advanced");
        Question question3 = new Question(3, "Java OOP");

        System.out.println("*** Persist - start ***");
        questionService.persist(question1);
        questionService.persist(question2);
        questionService.persist(question3);
        List<Question> questions1 = questionService.findAll();
        System.out.println("Questions Persisted are :");
        for (Question q : questions1) {
            System.out.println("-" + q.toString());
        }
        System.out.println("*** Persist - end ***");

        System.out.println("*** Update - start ***");
        question1.setText("Java Best Practices");
        questionService.update(question1);
        System.out.println("Question Updated is =>" +questionService.findById(question1.getId()).toString());
        System.out.println("*** Update - end ***");

        System.out.println("*** Find - start ***");
        Integer id1 = question1.getId();
        Question another = questionService.findById(id1);
        System.out.println("Question found with id " + id1 + " is =>" + another.toString());
        System.out.println("*** Find - end ***");

        System.out.println("*** Delete - start ***");
        Integer id3 = question3.getId();
        questionService.delete(id3);
        System.out.println("Deleted Question with id " + id3 + ".");
        System.out.println("Now all Questions are " + questionService.findAll().size() + ".");
        System.out.println("*** Delete - end ***");

        System.out.println("*** FindAll - start ***");
        List<Question> questions2 = questionService.findAll();
        System.out.println("Questions found are :");
        for (Question q : questions2) {
            System.out.println("-" + q.toString());
        }
        System.out.println("*** FindAll - end ***");

        System.out.println("*** DeleteAll - start ***");
        questionService.deleteAll();
        System.out.println("Questions found are now " + questionService.findAll().size());
        System.out.println("*** DeleteAll - end ***");
        System.exit(0);
    }
}
