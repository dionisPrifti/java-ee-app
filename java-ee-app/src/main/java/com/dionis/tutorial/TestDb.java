package com.dionis.tutorial;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

public class TestDb {

    public static void main(String[] args) {
        String dbClass = "com.mysql.jdbc.Driver";
        String dbUrl = "jdbc:mysql://localhost:3306/";
        String dbName = "online_testing";
        String username = "root";
        String password = "root";

        try{
            Class.forName(dbClass);
            String fullUrl = dbUrl + dbName;

            Connection con=DriverManager.getConnection(
                    fullUrl,username,password);

            String query = "select * from question";
            Statement stmt=con.createStatement();
            ResultSet rs=stmt.executeQuery(query);

            while(rs.next())
                System.out.println(rs.getInt(1)+"  "+rs.getString(2));
            con.close();

        } catch(Exception e){
            System.out.println(e);
        }
    }
}

