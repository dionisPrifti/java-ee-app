package com.dionis.tutorial.controllers;

import javax.faces.bean.ManagedBean;

@ManagedBean
public class HelloController {

    final String world = "Hello Internship!";

    public String getworld() {
        return world;
    }
}
